# Changelog

## 1.3.0 (unreleased)

-   Update MLy citation: doi:10.1103/PhysRevD.110.104034

-   Capitalize Bilby sky map names in test files.

-   Include missing test files in package.

-   Add descriptions of all tests and test events.

-   Reduce GraceDB calls by reusing info where possible.

-   Grab time difference info for coincidences via the VOEvent.

-   Add coarse-grained chirp mass information (CGMI) to circulars.

## 1.2.11 (2024-12-19)

-   Address Peter's comments from PnP regarding SSM circulars.

-   Fix bug where CWB BBH was not treated as a CBC in the subject.

-   Update gstlal citation after publication.

-   Switch over to pyproject.toml from setup.py, and drop setuptools,
    pkg_resources, and versioneer.py to support python 3.12. Use more standard
    tools for pytest in CI templates. Drop python 3.9.

-   Add tests to explicitly check the text output matches and expected set of
    templates. Update tests to use multi-ordered sky maps, to both speed up
    these tests and be closer to the current low-latency system. Remove line
    break tests since these are covered by explicit checking now.

-   Require python 3.12 tests to pass. Add python 3.13 tests but allow to fail
    until the LSC software stack supports it.

-   Include each EM bright sentence if the respective value is available.

-   Create a pdf in the CI pipeline using the fixed templates, both to monitor
    these as examples and upload the file to DCC for P&P review.

-   Address comments from P&P review.

-   Address feedback for RAVEN circulars due to S241125n by removing Swift
    sky map text, indicate joint FAR is reported before trials, address
    external events as candidates, and soften language using LIGO data if only
    updating external coincidence info.

## 1.2.10 (2024-07-08)

-   Added `HasSSM` source property and made necessary changes to 
    both initial and update circulars.

-   Use unique citation for CWB BBH events.

-   Fixed an incorrect DOI for one of the citations.

-   Make SCCB ticket creation a manual process. Update README.

-   Update bilby citation to published version.

-   Fix test causing docker build to fail; add python 3.12 tests.

## 1.2.9 (2024-05-03)

-   Add additional RAVEN citation.

-   Fixing spacing issues with combined sky maps.

-   If the preferred event is a Burst CWB BBH event, treat like a CBC and use
    the appropriate language as such.

## 1.2.8 (2023-10-31)

-   Add DOIs to citations.

-   Update URL of userguide to shortened version.

-   Change wording of HassMassgap to HasMassGap.

-   Update versioneer and setup.py to fix failing builds.

## 1.2.7 (2023-08-23)

-   Update RapidPE-RIFT citation.

-   Add check to find which pipeline supplied p_astro numbers. Update wording
    if p_astro was supplied by RapidPE. 

## 1.2.6 (2023-08-09)

-   Add ROQ reference to Bilby update circular.

-   Add option to not wrap lines in order to follow current GCN circular
    guidelines. Fix command-line options for compose_update in order to include
    multiple update types separated by commas.

-   Update gstlal citation to published version.

## 1.2.5 (2023-07-10)

-   Grab p_astro and em_bright info from VOEvents to ensure the circular matches
    the values sent in the alert.

-   Sort pipelines using key that ignores capitalization.

-   Vectorize crossmatch for ellipse calculation. Drop python 3.8.

-   List GRB collaboration as co-author if SubGRBTargeted detection.

## 1.2.4 (2023-06-20)

-   Mention article number in place of issue number for em-bright reference.
    Change issue number 1 to article number 54 in
    https://iopscience.iop.org/article/10.3847/1538-4357/ab8dbe

-   Address various comments for text given from P&P.

-   Add arguments to override whether to create a detection or exclusion medium
    latency GRB circular.

-   Require sky map area to match ellipse at the 50% level in addition to the
    current 90% to be considered an ellipse.

## 1.2.3 (2023-05-24)

-   Shorten Astrophysical Journal to ApJ in refs.

-   Update gstlal citation to be ArXiv versions.

-   Update SPIIR citation.

-   Specify a sky map is from early warning and fix timing for events with
    negative latencies.

## 1.2.2 (2023-05-03)

-   Add preliminary version of LLAMA circular templates, created using new
    compose_llama function.

-   Update MBTA reference.

-   Update guess_loc_pipeline function to be more flexible and fail-safe.

-   Add text for pipelines that found early warning event.

-   Order pipelines by alphabetical rather than preferred event first.

-   Add RapidPE-RIFT citation.

-   Fix MassGap by moving to em_bright section and removing from p_astro
    example files.

-   Ensure MDC SNEWS events compile correctly by fixing a bug where they were
    misidentified as GRB events.

-   Update em_bright section to reflect current methods.

-   Update GstLAL citation.

## 1.2.1 (2023-04-20)

-   Fix bug where pipelines didn't appear yet in pipeline preferred events,
    causing the circulars to miss pipelines or fail.

-   Fix example of medium-latency PyGRB exclusion.

-   Update PyCBC citation.

## 1.2.0 (2023-04-16)

-   Add KAGRA to templates and description.

-   Add medium latency test using only PyGRB to ensure our template works for
    this example.

-   Fix broken citations by using a universal citation index dictionary.

-   Reference all combined sky maps previously sent in VOEvents, similarly
    to how we reference all GW sky maps.

-   Remove some GraceDB polls and update files to be similar to what is
    expected in O4.

-   Add Burst-SNEWS coincidence to example templates.

-   Add text to describe SubGRBTargeted GRB candidate if present.

-   Raise a ValueError if no VOEvents present to get data from.

-   Add text to mention all other external event detections as well.

-   Add keyword argument for ratio of ellipse area and 90% containment area.

-   Add description of RA, dec, and error radius when a Swift coincidence.

-   Add example of a retraction of an early warning candidate.

## 1.1.10 (2022-08-19)

-   Add RAVEN coincidence to update types.

-   Increase mimumum python version to 3.8.

## 1.1.9 (2021-08-03)

-   Fix the CI so that PackageLoader works correctly with jinja.

## 1.1.8 (2020-04-21)

-   Track a function rename in ligo.skymap. The function
    `ligo.skymap.postprocess.find_injection.find_injection_moc` was renamed to
    `ligo.skymap.postprocess.crossmatch.crossmatch`. The `find_injection_moc`
    function was deprecated in ligo.skymap 0.1.10 in August 2019, and was
    finally removed in ligo.skymap 0.2.0 in April 2020.

## 1.1.7 (2020-03-24)

-   Fix bug where grabbing a combined sky map can return a .png of
    the same filename.

## 1.1.6 (2020-02-26)

-   Fix missing whitespace introduced after Jinja2 updated to 2.11.0.

-   Adjust ellipse rounding to including more significant digits.

## 1.1.5 (2020-02-22)

-   Include Bilby in sky map citations.

-   Update method to get combined sky map to include Swift, INTEGRAL, and
    AGILE MCAL.

-   Include trials to determine whether a gw event is significant enough on
    its own in a RAVEN coincidence.

## 1.1.4 (2019-12-18)

-   Fix RAVEN circular to only run p_astro and em_bright when a CBC.

## 1.1.3 (2019-10-18)

-   Fix bug where a preliminary circular lists the sky map as being an
    updated localization.

-   Change wording to explicity state that coincidence is significant, not
    that the GW increases in significance. Include joint sky map in both
    EM COINC circulars.

-   Fix sentence in description of sky map so it doesn't appear cut-off.

-   Change EM_Bright section to simply state the probability of HasNS
    and HasRemnant instead of relative amount of evidence.

-   Get p_astro and em_bright files from the superevent instead of the
    preferred event in order to accomodate PE-based results.

-   Get the preferred external event from 'em_type' field in superevent,
    if no value grab first external event.

## 1.1.2 (2019-09-25)

-   Remove check that sky map has `public` and `sky_loc` tags in order to
    fix bug preventing finding files with version numbers in their extension
    (e.g. LALinference.fits.gz,0)

## 1.1.1 (2019-09-11)

-   Change wording for PE p_astro to include the assumption that the
    candidate is astrophysical. Change update circular wording to include
    GCN circular by hand.

-   Change retraction wording to 'no longer considered a candidate of interest'.

## 1.1.dev1 (2019-08-21)

-   Add update type circular template. This template can be used for any
    combination of updated sky localization, source classification (p_astro),
    and EM-bright classification.

-   Uses search field to identify sub-threshold GRBs.

## 1.0.2 (2019-06-06)

-   Changed to use multiorder.fits sky map first when calculating uncertainty ellipses.

-   Removed far threshold criteria for pipelines to be included in circular.

## 1.0.1 (2019-05-30)

-   New release with no changes to fix incorrect version number on PyPI.

## 1.0.0 (2019-05-30)

-   Updated SPIIR reference to Chichi's thesis.

-   Changed the FAR statement to say it is estimated (rather than "determined").

-   Added far threshold for pipelines to be included.

-   Change "skymap" to "sky map".

-   Have SPIIR display as uppercase in Circular text.

## 0.0.28 (2019-04-12)

-   Updated macros to state FAR explicitly; no more capping at 1/century.

## 0.0.27 (2019-04-10)

-   Updated wording in initial circular to match what was sent for the first
    superevent alert sent in O3

-   Updated wording for the RAVEN circulars; particularly when there is a
    combined GW-GRB sky map.

-   Updated to call gstlal as GstLAL when referring to pipelines.

-   Added citations for medium latency GRB-GW search pipelines.

## 0.0.26 (2019-04-09)

-   Updated to call EM-Bright json file from the preferred event files list;
    probabilities are between 0 and 1

## 0.0.25 (2019-04-08)

-   Updated reference for PyCBC Live.

-   Updated wording for the retraction circular.

-   Updated wording for the RAVEN circulars.

-   Updated wording for the medium latency GRB circulars.

-   Added sky map citations for BAYESTAR and LALInference.

## 0.0.24 (2019-03-20)

-   Moved pipeline citations from the circular body to the end and enumerated.

-   Updated the p_astro.json test files to include the mass gap.

-   Synced (but hard-coded) thresholds for sending alerts for CBC and Burst
    superevents.

## 0.0.23 (2019-03-08)

-   Added ability to pull down excluded distance json files created by
    PyGRB and X-pipeline to put into exclusion medium latency GRB circulars.

-   Updated source_classification.json to em_bright.json.

## 0.0.22 (2019-03-07)

-   Updated RAVEN specific circulars to handle sub-threshold GRB triggers.

-   Created medium latency GRB circulars if X-pipeline or PyGRB find a
    coincident GW.

-   Added luminosity distance to the initial circular body when available.

## 0.0.21 (2019-02-27)

-   Un-fixed the "correct" sky map name... i.e., will parse VOEvents for
    skymap_fits param.

-   Updated the code to use the multi-resolution sky map if available.

## 0.0.20 (2019-02-27)

-   Changed LIB to oLIB.

## 0.0.19 (2019-02-27)

-   Added citations for other GW analysis pipelines that find the event other
    than the preferred event pipeline.

-   Fixed how the correct sky map name is deduced.

## 0.0.18 (2019-02-25)

-   Re-worded part of the EM_COINC circulars that gives the time difference
    between the neutrino/GRB trigger and GW candidate event.

## 0.0.17 (2019-02-25)

-   Fixed an unfortunate typo.

## 0.0.16 (2019-02-25)

-   Improved the EM_COINC and initial circulars.

## 0.0.15 (2019-02-22)

-   Added template for EM_COINC circulars, i.e., circulars produced when
    RAVEN finds a coincidence between GW and external triggers.

## 0.0.14 (2019-02-13)

-   Added template for retraction circulars.

## 0.0.13.dev0 (2019-02-13)

-   Update the XPath expression for obtaining the sky map URL to conform to the
    new schema for O3. This should fix the missing sky map paragraphs in
    circulars that are uploaded to GraceDb.

## 0.0.12 (2019-02-12)

-   Circulars include p_astro information when available.

## 0.0.11 (2018-08-02)

-   Circulars are generated with uncertainty ellipse text when they are a good
    approximation.

## 0.0.10 (2018-06-28)

-   Changed how VOEvent text is pulled down; now using `client.files()` method
    versus relying on `client.voevents()` json response.

## 0.0.9 (2018-06-28)

-   Circulars are generated strictly for superevents.

## 0.0.8 (2018-06-27)

-   GraceDb links in circulars now reflect the URL of the GraceDb server from
    which the event originated.

## 0.0.7 (2018-05-24)

-   Drop support for Python 2.

-   Do not print the circular if calling `compose()` from a Python script.

-   Remove duplicate sky maps and keep only the lowest latency ones.

-   Do not include sky maps produced outside the LVC.

-   Optional GraceDb client keyword argument when calling compose().

## 0.0.6 (2018-05-08)

-   Make it easier to call functions from Python scripts, like this:

        >>> from ligo import followup_advocate
        >>> text = followup_advocate.compose('G299232')

-   Add `--service` command line option to set the GraceDB service URL.

## 0.0.5 (2018-05-03)

-   First version released on PyPI.
