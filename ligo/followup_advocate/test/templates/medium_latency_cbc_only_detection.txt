SUBJECT: INTEGRAL candidate with ID 206740: Identification of a possible gravitational-wave counterpart

The LIGO Scientific Collaboration, the Virgo Collaboration, and the KAGRA
Collaboration report:

The PyGRB binary merger analysis [1] made a preliminary identification of a
gravitational wave (GW) candidate associated with the time and sky position of
GRB INTEGRAL candidate with ID 206740 **CITE ORIGINAL GCN FOR THE EXTERNAL
CANDIDATE FROM https://gcn.nasa.gov/circulars, e.g., (Bhalerao et al., GCN
Circular XXXXX)**.

The candidate is consistent with a binary coalescence with False Alarm
Probability of 0.0065522.

The False Alarm Probability is calculated by counting the fraction of
background trials containing an event with a greater detection statistic than
the loudest on-source event. The off-source trials are of 6 s duration.

 [1] Williamson et al. PRD 90, 122004 (2014) doi:10.1103/PhysRevD.90.122004