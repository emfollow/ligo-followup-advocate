SUBJECT: LIGO/Virgo/KAGRA S6789: Identification of a GW compact binary merger candidate

The LIGO Scientific Collaboration, the Virgo Collaboration, and the KAGRA
Collaboration report:

We identified the compact binary merger candidate S6789 during real-time
processing of data from LIGO Hanford Observatory (H1) and LIGO Livingston
Observatory (L1) at 2018-06-28 03:08:04.741 UTC (GPS time: 1214190502.741). The
candidate was found by the MBTA [1] analysis pipeline.

S6789 is an event of interest because its false alarm rate, as estimated by the
online analysis, is 9.1e-07 Hz, or about one in 12 days. The event's properties
can be found at this URL:

https://gracedb.invalid/superevents/S6789

The analysis which found this candidate used a template  bank including sub-
solar-mass components. The usual classification probabilities for BNS, NSBH, or
BBH binaries versus Terrestrial noise are not calculated for this candidate.
The probability of matter remaining outside the final compact object
(HasRemnant) [2] is not estimated either.

Assuming the candidate is astrophysical in origin, the probability that at
least one of the compact objects is consistent with a neutron star mass between
1 and 3 solar masses (HasNS) is >99%. [2] The probability that either of the
binary components lies between 3 and 5 solar masses (HasMassGap) is <1%. The
probability that the lighter compact object is below 1 solar mass (HasSSM) is
50%.

One sky map is available at this time and can be retrieved from the GraceDB
event page:
 * bayestar.multiorder.fits,0, an initial localization generated by BAYESTAR
[3], distributed via GCN notice about 10 hours after the candidate event time.

For the bayestar.multiorder.fits,0 sky map, the 90% credible region is 1079
deg2. Marginalized over the whole sky, the a posteriori luminosity distance
estimate is 158 +/- 44 Mpc (a posteriori mean +/- standard deviation).

For further information about analysis methodology and the contents of this
alert, refer to the LIGO/Virgo/KAGRA Public Alerts User Guide
https://emfollow.docs.ligo.org/.

 [1] Aubin et al. CQG 38, 095004 (2021) doi:10.1088/1361-6382/abe913
 [2] Chatterjee et al. ApJ 896, 54 (2020) doi:10.3847/1538-4357/ab8dbe
 [3] Singer & Price PRD 93, 024013 (2016) doi:10.1103/PhysRevD.93.024013