SUBJECT: Swift/BAT candidate with ID 876016: Identification of a possible gravitational-wave counterpart

The LIGO Scientific Collaboration, the Virgo Collaboration, and the KAGRA
Collaboration report:

The X-Pipeline GRB-unmodeled transient analysis [1] and PyGRB binary merger
analysis [2] made preliminary identification of gravitational wave (GW)
candidates associated with the time and sky position of GRB Swift/BAT candidate
with ID 876016 **CITE ORIGINAL GCN FOR THE EXTERNAL CANDIDATE FROM
https://gcn.nasa.gov/circulars, e.g., (Bhalerao et al., GCN Circular XXXXX)**.

In PyGRB, the candidate is consistent with a binary coalescence with False
Alarm Probability of 6.6e-02.

In X-Pipeline, the candidate is an unmodeled GW transient with False Alarm
Probability of 2.4e-03.

The False Alarm Probability is calculated by counting the fraction of
background trials containing an event with a greater detection statistic than
the loudest on-source event. The off-source trials are of 6 s duration for CBC
and 660 s duration for Burst.

 [1] Was et al. PRD 86, 022003 (2012) doi:10.1103/PhysRevD.86.022003
 [2] Williamson et al. PRD 90, 122004 (2014) doi:10.1103/PhysRevD.90.122004